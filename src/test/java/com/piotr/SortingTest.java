package com.piotr;

import org.junit.Assert;
import org.junit.Test;

public class SortingTest {

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgs() {
        String[] args = {};

        Sorting.sort(args);
    }

    @Test
    public void testOneArg() {
        String[] args = {"21"};

        Assert.assertEquals("21", Sorting.sort(args));
    }

    @Test
    public void testTenArgs() {
        String[] args = {"3", "-8", "21", "96", "-723", "13", "420", "2", "-100", "61"};
        String expected = "-723 -100 -8 2 3 13 21 61 96 420";

        Assert.assertEquals(expected, Sorting.sort(args));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooManyArgs() {
        String[] args = {"42", "10", "-52", "91", "61", "421", "-75", "8", "-3", "39", "100"};

        Sorting.sort(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonIntArgs() {
        String[] args = {"8", "-3", "39", "100", "13.5", "five", "36.6"};

        Sorting.sort(args);
    }
}