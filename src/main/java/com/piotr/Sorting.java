package com.piotr;

import java.util.Arrays;

public class Sorting {

    public static String sort(String[] numbers) {
        int size = numbers.length;
        int[] intNumbers = new int[size];

        if (size > 10 || size == 0) {
            throw new IllegalArgumentException("There should be between 1 and 10 arguments.");
        }

        try {
            for (int i = 0; i < size; i++) {
                intNumbers[i] = Integer.parseInt(numbers[i]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Arguments should be integer values.");
        }

        Arrays.sort(intNumbers);

        StringBuilder outputNumbers = new StringBuilder();
        for (int i = 0; i < intNumbers.length; i++) {
            outputNumbers.append(intNumbers[i]);
            if (i < intNumbers.length - 1) {
                outputNumbers.append(" ");
            }
        }

        return outputNumbers.toString();
    }
}